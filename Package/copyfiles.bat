::: Copy files into /bin folder

@echo off

:::: First, Copy Application DLLs
echo "Copying MathEasy DLLs.."

IF EXIST "%MATHEASY_BUILD%" (

    copy %MATHEASY_BUILD%\App.exe bin\
    copy %MATHEASY_BUILD%\LogicLib.dll bin\
    copy %MATHEASY_BUILD%\ToolBox.dll bin\
    copy %MATHEASY_BUILD%\MathFunction.dll bin\
    copy %MATHEASY_BUILD%\METoolFactory.dll bin\

) else (
    echo "MathEasy Build Directory not found !!"
)

:::: Second, Copy QT DLLs
echo "Copying QT DLLs.."

IF EXIST "%QT_PATH%" (
    copy %QT_PATH%\Qt5Core.dll bin\
        copy %QT_PATH%\icuin53.dll bin\
        copy %QT_PATH%\icuuc53.dll bin\
        copy %QT_PATH%\icudt53.dll bin\
        copy %QT_PATH%\Qt5Quick.dll bin\
        copy %QT_PATH%\Qt5Gui.dll bin\
        copy %QT_PATH%\Qt5Qml.dll bin\
        copy %QT_PATH%\Qt5Network.dll bin\
		copy %QT_PATH%\libgcc_s_dw2-1.dll bin\
		copy "%QT_PATH%\libstdc++-6.dll" bin\
		copy %QT_PATH%\libwinpthread-1.dll bin\
) else (
    echo "QT 5.4 Directory not found !!"
)
