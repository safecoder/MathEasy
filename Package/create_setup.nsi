; Math Easy Create Installer
; Author: Safeer
; Date: 11 May 2015
 
 
; -------------------------------
; Start
 
  !define APP_NAME "MathEasy"
  !define VERSION "1.0"
  !define PRODUCT_NAME "${APP_NAME}-${VERSION}"
  !define SETUP_FILE_NAME "MathEasy.exe"
  !define BRANDINGTEXT "MathEasy Ver. 1.0"
  CRCCheck On
 
  !include "${NSISDIR}\Contrib\Modern UI\System.nsh"
 
 
;---------------------------------
;General
 
;Include Modern UI
!include "MUI2.nsh"

  Name "${PRODUCT_NAME}"
  OutFile "${SETUP_FILE_NAME}"
  ShowInstDetails "show"
  ShowUninstDetails "show"
  SetCompressor /SOLID lzma
  SetCompressorDictSize 12
  ;SetCompressor "bzip2"
 
  !define SETUP_ICON "resource\MathEasy.ico"
  !define SETUP_UNICON "resource\MathEasy.ico"
  ;!define MATHEASY_SPECIALBITMAP "Bitmap.bmp"
 
 
;--------------------------------
;Folder selection page
 
  InstallDir "$PROGRAMFILES\${PRODUCT_NAME}"
 
 
;--------------------------------
;Modern UI Configuration
 
 !define MUI_ICON "${SETUP_ICON}"
 !define MUI_UNICON "${SETUP_UNICON}"
 
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "resource\License.txt"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages

!insertmacro MUI_LANGUAGE "English"

;-------------------------------- 

; Temporary folder for dlls
!define TEMPFOLDER "bin"


;Installer Sections     
Section "install" Installation_info
 
;Add files
  SetOutPath "$INSTDIR"
 
  File "${TEMPFOLDER}\App.exe"
  File "${TEMPFOLDER}\*.dll"
  ;File "${TEMPFOLDER}\LogicLib.dll"
  /*
  File "${TEMPFOLDER}\Qt5Core.dll"
  File "${TEMPFOLDER}\icuin53.dll"
  File "${TEMPFOLDER}\icuuc53.dll"
  File "${TEMPFOLDER}\icudt53.dll"
  File "${TEMPFOLDER}\Qt5Quick.dll"
  File "${TEMPFOLDER}\Qt5Gui.dll"
  File "${TEMPFOLDER}\Qt5Qml.dll"
  File "${TEMPFOLDER}\Qt5Network.dll"
  */
	
  File "resource\ReadMe.txt"
 
;create desktop shortcut
  CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\App.exe" ""
 
;create start-menu items
  CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME}"
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk" "$INSTDIR\App.exe" "" "$INSTDIR\App.exe" 0

;write uninstall information to the registry
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "DisplayName" "${PRODUCT_NAME} (remove only)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "UninstallString" "$INSTDIR\Uninstall.exe"
 
  WriteUninstaller "$INSTDIR\Uninstall.exe"
 
SectionEnd
 
 
;--------------------------------    
;Uninstaller Section  
Section "Uninstall"
 
;Delete Files 
  RMDir /r "$INSTDIR\*.*"    
 
;Remove the installation directory
  RMDir "$INSTDIR"
 
;Delete Start Menu Shortcuts
  Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\*.*"
  RmDir  "$SMPROGRAMS\${PRODUCT_NAME}"
 
;Delete Uninstaller And Unistall Registry Entries
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\${PRODUCT_NAME}"
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
 
SectionEnd
 
 
;--------------------------------    
;MessageBox Section
 
 
;Function that calls a messagebox when installation finished correctly
Function .onInstSuccess
  MessageBox MB_OK "You have successfully installed ${PRODUCT_NAME}. Use the desktop icon to start the program."
FunctionEnd
 
 
Function un.onUninstSuccess
  MessageBox MB_OK "You have successfully uninstalled ${PRODUCT_NAME}."
FunctionEnd
 
 
;eof