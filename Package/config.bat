::::::: CONFIGURATION TO SET UP REQUIRED PATHS

:: QT 5.4 Bin Directory Path
set QT_PATH=C:\Qt\Qt5.4.1\5.4\mingw491_32\bin

:: MathEasy Build Folder Path
set MATHEASY_BUILD=..\..\Build_MathEasy\release

:: NSIS Installation Directory
set NSIS_PATH=C:\Program Files (x86)\NSIS
