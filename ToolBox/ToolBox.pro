#-------------------------------------------------
#
# Project created by QtCreator 2015-05-26T11:24:55
#
#-------------------------------------------------

# build setup
include(../Config/build.pri)

QT       -= gui
QT += qml quick

TARGET = ToolBox
TEMPLATE = lib

DEFINES += TOOLBOX_LIBRARY

SOURCES += \
    ToolBox.cpp \
    ToolBoxFactory.cpp \
    Connector.cpp

HEADERS +=\
        toolbox_global.h \
        IToolBox.h \
        ToolBox.h \
    ToolBoxFactory.h \
    Connector.h \
    IConnector.h \
    ToolBoxDefs.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

INCLUDEPATH += $$PWD/..

