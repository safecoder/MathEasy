#ifndef ME_CONNECTOR_H
#define ME_CONNECTOR_H

#include <ToolBox/IConnector.h>
#include <QObject>

namespace ME_ToolBox {

    class Connector: public QObject,
                     public IConnector
    {
    Q_OBJECT
    public:
        Connector(QObject* parent=0);

        ~Connector();

        /// \brief  Initialize this connector
        void initialize(QQmlApplicationEngine* engine);

        /// \brief  Get QML Item associated with it
        QQuickItem* getQMLItem();

        /// \brief  Get the unique Id generated when this Item was created
        const QString& getUniqueId() const;

        /// \brief  Set Tool Box's which are connected either left or right
        void setToolBox(QSharedPointer<IToolBox> tb, const ToolBoxConnection& connection);

        /// \brief  Unsets Tool Box's which are connected either left or right
        void unsetToolBox(const ToolBoxConnection& connection);

        /// \brief  Sets the connection state, so that connector UI gets
        ///         Its shape and color w.r.t the sate
        void setConnectorState(const ConnectionState& state);

        /// \brief  Resets connection
        void reset();

    signals:

        /// \brief  Signal for destroying this connector
        void remove(QString);

    private slots:

        /// \brief  Slot for handling button click events from the connector
        void _handleClicks(const QString& buttonName, const QString& value);

        /// \brief  Slot for handling its destruction
        void _handleClosing();

        /// \brief  Slot for resetting a connection
        void _handleConnectionReset(const int& rToolBoxExists);

    protected:

        /// \brief  QML Item associated with it
        QQuickItem* _guiItem;

        /// \brief  Unique Id for this connector
        QString _uniqueId;

        /// \brief  ToolBox's Connected to this Item
        QSharedPointer<IToolBox> _leftToolBox, _rightToolBox;
    };
}


#endif // ME_CONNECTOR_H
