#ifndef ME_TOOLBOX_H
#define ME_TOOLBOX_H

#include <ToolBox/IToolBox.h>
#include <QObject>

namespace ME_ToolBox {

    class TOOLBOXSHARED_EXPORT ToolBox:
                    public QObject,
                    public IToolBox
    {
    Q_OBJECT
    public:
        ToolBox(QObject* parent=0);

        ~ToolBox();

        /// \brief  Initialize this tool box
        void initialize(QQmlApplicationEngine* engine, QQuickItem* parent, int x, int y);

        /// \brief  Get QML Item associated with it
        QQuickItem* getQMLItem();

        /// \brief  setter for unique name
        void setUniqueName(const QString& uname);

        /// \brief  getter for unique name
        const QString& getUniqueName() const;

        /// \brief  setter for Tool Box Type
        void setToolBoxType(const QString& type);

        /// \brief  getter for Tool Box Type
        const QString& getToolBoxType() const;

        /// \brief  Does this tool box has input pin
        bool hasInputPin();

        /// \brief  Does this tool box has output pin
        bool hasOutputPin();

        /// \brief  Set Title for this Tool Box
        void setTitle(const QString& title);

        /// \brief  Add a Connector to the output pin of this Tool Box
        bool addConnectorToOutputPin(IConnector* connector);

        /// \brief  Set Tool Box's which are connected either left or right
        void setToolBox(QSharedPointer<IToolBox> tb, const ToolBoxConnection& connection);

        /// \brief  Unsets Tool Box's which are connected either left or right
        void unsetToolBox(const ToolBoxConnection& connection);

    signals:

        /// \brief  Signal for closing the ToolBox
        void closeToolBoxSignal(QString);

    private slots:

        /// \brief  Slot for handling button click events from Tool Box
        void _handleClicks(const QString& buttonName, const QString& value);

    protected:

        /// \brief  Add Tool Box into its parent item
        void _addToParentItem(QQuickItem* parent, int x, int y);

        /// \brief  QML Item associated with it
        QQuickItem* _guiItem;

        /// \brief Unique name by which this tool box is identified by the Application
        QString _uniqueName;

        /// \brief  Type of the Tool Box
        QString _type;

        /// \brief  ToolBox's Connected to this Item
        QSharedPointer<IToolBox> _leftToolBox, _rightToolBox;
    };
}


#endif // ME_TOOLBOX_H
