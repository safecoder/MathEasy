#include <ToolBox/ToolBoxFactory.h>
#include <ToolBox/ToolBox.h>
#include <ToolBox/Connector.h>

namespace ME_ToolBox {
    QObject*
    ToolBoxFactory::createObj(const ObjType& type)
    {
        if(type == ToolBoxType)
        {
            // We may not be using this because this class
            // is an abstraction for Tool Boxes. But still can be used
            // For testing.
            return (new ToolBox(NULL));
        }
        else if(type == ConnectorType)
        {
            return (new Connector(NULL));
        }
        return NULL;
    }
}
