#include <ToolBox/ToolBox.h>
#include <assert.h>
#include <QThread>
#include <QDebug>
#include <ToolBox/IConnector.h>

namespace ME_ToolBox {

    ToolBox::ToolBox(QObject* parent):
    QObject(parent)
    ,_uniqueName("")
    ,_type("")
    {
    }

    ToolBox::~ToolBox()
    {
        _guiItem->setParentItem(0);
        _guiItem->deleteLater();
    }

    void
    ToolBox::initialize(QQmlApplicationEngine* engine, QQuickItem* parent, int x, int y)
    {
        QQmlComponent aTemplate(engine, QUrl("qrc:///ToolBoxWrapper.qml"));
        _guiItem = qobject_cast<QQuickItem*>(aTemplate.create());
        assert(_guiItem);

        // As soon as we create it, add it into the parent item
        _addToParentItem(parent, x, y);

        QQmlEngine::setObjectOwnership(_guiItem, QQmlEngine::CppOwnership);

        // Listen to the signals
        bool connected = connect(dynamic_cast<QObject*>(_guiItem),
                                 SIGNAL(buttonSignal(QString, QString)),
                                 this,
                                 SLOT(_handleClicks(QString, QString)));

        assert(connected);

    }

    void
    ToolBox::_addToParentItem(QQuickItem* parent, int x, int y)
    {
        assert(parent);
        assert(_guiItem);

        x -= _guiItem->width()*0.5;
        y -= _guiItem->height()*0.5;

        // Modify x,y to fit exactly inside the playground
        if(x < 0)
        {
            x = 0;
        }
        else if(x > parent->width() - _guiItem->width())
        {
            x = parent->width() - _guiItem->width();
        }

        if(y < 0)
        {
            y = 0;
        }
        else if(y > parent->height() - _guiItem->height())
        {
            y = parent->height() - _guiItem->height();
        }

        _guiItem->setX(x);
        _guiItem->setY(y);
        _guiItem->setParentItem(parent);
    }

    void
    ToolBox::_handleClicks(const QString& buttonName, const QString& value)
    {
        Q_UNUSED(value)

        if(buttonName == "close")
        {
            // Send a signal to Root Adapter for closing this tool box
            emit closeToolBoxSignal(_uniqueName);
        }
    }

    void
    ToolBox::setUniqueName(const QString& uname)
    {
        _uniqueName = uname;

        // Set this value in the QML Item as well
        if(_guiItem)
        {
            _guiItem->setProperty("uniqueName", QVariant(uname));
        }
    }

    const QString&
    ToolBox::getUniqueName() const
    {
        return _uniqueName;
    }

    void
    ToolBox::setToolBoxType(const QString& type)
    {
        _type = type;
    }

    const QString&
    ToolBox::getToolBoxType() const
    {
        return _type;
    }

    QQuickItem*
    ToolBox::getQMLItem()
    {
        return _guiItem;
    }

    bool
    ToolBox::hasInputPin()
    {
        return false;
    }

    bool
    ToolBox::hasOutputPin()
    {
        return false;
    }

    void
    ToolBox::setTitle(const QString& title)
    {
        if(_guiItem)
        {
            _guiItem->setProperty("title", QVariant(title));
        }
    }

    bool
    ToolBox::addConnectorToOutputPin(IConnector* connector)
    {
        //  Add the connector to the Tool Box UI, Connector will appear at
        //  the right output pin of the Tool Box
        if(_guiItem)
        {
            QVariant returnedValue;
            QVariant connectorIdValue = QVariant(connector->getUniqueId());
            QMetaObject::invokeMethod((QObject*)_guiItem, "addConnector",
                    Q_RETURN_ARG(QVariant, returnedValue),
                    Q_ARG(QVariant, connectorIdValue));

            bool outcome = returnedValue.toBool();
            return outcome;
        }

        return false;
    }

    void
    ToolBox::setToolBox(QSharedPointer<IToolBox> tb, const ToolBoxConnection& connection)
    {
        if(tb)
        {
            if(connection==LEFT_TOOLBOX)
            {
                _leftToolBox = tb;
            }
            else if(connection==RIGHT_TOOLBOX)
            {
                _rightToolBox = tb;
            }
        }
        else
        {
            qCritical() <<_uniqueName << ": setToolBox() NULL pointer";
        }
    }

    void
    ToolBox::unsetToolBox(const ToolBoxConnection& connection)
    {
        if(connection==LEFT_TOOLBOX)
        {
            _leftToolBox = QSharedPointer<IToolBox>(NULL);
        }
        else if(connection==RIGHT_TOOLBOX)
        {
            _rightToolBox = QSharedPointer<IToolBox>(NULL);
        }
    }
}
