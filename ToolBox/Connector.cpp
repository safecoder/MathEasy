#include <ToolBox/Connector.h>
#include <ToolBox/IToolBox.h>
#include <assert.h>
#include <QDebug>
#include <QThread>

namespace ME_ToolBox {

    Connector::Connector(QObject* parent):
    QObject(parent)
    {
    }

    Connector::~Connector()
    {
        _guiItem->setParentItem(0);
        _guiItem->deleteLater();
    }

    void
    Connector::initialize(QQmlApplicationEngine* engine)
    {

        QQmlComponent aTemplate(engine, QUrl("qrc:///ConnectorPipe.qml"));
        _guiItem = qobject_cast<QQuickItem*>(aTemplate.create());
        QThread::sleep(1);
        assert(_guiItem);

        QQmlEngine::setObjectOwnership(_guiItem, QQmlEngine::CppOwnership);

        // Get the unique Id generated while creating this
        QVariant uniqueId = _guiItem->property("uniqueId");
        _uniqueId = uniqueId.toString();

        // Listen to the signals
        bool connected = connect(dynamic_cast<QObject*>(_guiItem),
                                 SIGNAL(closeSignal()),
                                 this,
                                 SLOT(_handleClosing()));

        assert(connected);

        connected = connect(dynamic_cast<QObject*>(_guiItem),
                                 SIGNAL(resetConnection(int)),
                                 this,
                                 SLOT(_handleConnectionReset(int)));

        assert(connected);
    }

    void
    Connector::_handleConnectionReset(const int& rToolBoxExists)
    {
        if(!_leftToolBox.isNull())
        {
            _leftToolBox->unsetToolBox(RIGHT_TOOLBOX);
        }

        if(rToolBoxExists && !_rightToolBox.isNull())
        {
            _rightToolBox->unsetToolBox(LEFT_TOOLBOX);
        }

        unsetToolBox(ME_ToolBox::RIGHT_TOOLBOX);
        setConnectorState(ME_ToolBox::DISCONNECTED);
    }

    void
    Connector::_handleClosing()
    {
        emit remove(_uniqueId);
    }

    void
    Connector::_handleClicks(const QString& buttonName, const QString& value)
    {
        Q_UNUSED(value)

        /*
        if(buttonName == "close")
        {
            // Send a signal to Root Adapter for closing this tool box
            emit closeToolBoxSignal(_uniqueName);
        }
        */
    }

    QQuickItem*
    Connector::getQMLItem()
    {
        return _guiItem;
    }

    const QString&
    Connector::getUniqueId() const
    {
        if(_guiItem)
        {
            return _uniqueId;
        }
        return QString("");
    }

    void
    Connector::setConnectorState(const ConnectionState& state)
    {
        QString stateValue;
        if(state == DISCONNECTED)
        {
            stateValue = "disconnected";
        }
        else if(state == IN_PROGRESS)
        {
            stateValue = "in_progress";
        }
        else if(state == CONNECTED)
        {
            stateValue = "connected";
        }
        else if(state == FAILED)
        {
            stateValue = "failed";
        }
        else
        {
            return;
        }

        //  Pass the state value to the Connector QML
        if(_guiItem)
        {
            QVariant returnedValue;
            QMetaObject::invokeMethod((QObject*)_guiItem, "changeConnectorState",
                    Q_RETURN_ARG(QVariant, returnedValue),
                    Q_ARG(QVariant, QVariant(stateValue)));
        }
    }

    void
    Connector::setToolBox(QSharedPointer<IToolBox> tb, const ToolBoxConnection& connection)
    {
        if(tb)
        {
            if(connection==LEFT_TOOLBOX)
            {
                _leftToolBox = tb;
            }
            else if(connection==RIGHT_TOOLBOX)
            {
                _rightToolBox = tb;
            }
        }
        else
        {
            qCritical() << _uniqueId << ": setToolBox() NULL pointer";
        }
    }

    void
    Connector::unsetToolBox(const ToolBoxConnection& connection)
    {
        if(connection==LEFT_TOOLBOX)
        {
            _leftToolBox = QSharedPointer<IToolBox>(NULL);
        }
        else if(connection==RIGHT_TOOLBOX)
        {
            _rightToolBox = QSharedPointer<IToolBox>(NULL);
        }
    }

    void
    Connector::reset()
    {
        // Reset Assumes that the Right ToolBox is valid
        _handleConnectionReset(1);
    }
}
