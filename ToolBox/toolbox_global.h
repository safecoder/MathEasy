#ifndef TOOLBOX_GLOBAL_H
#define TOOLBOX_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(TOOLBOX_LIBRARY)
#  define TOOLBOXSHARED_EXPORT Q_DECL_EXPORT
#else
#  define TOOLBOXSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // TOOLBOX_GLOBAL_H
