#ifndef ME_TOOLBOXDEFS_H
#define ME_TOOLBOXDEFS_H


namespace ME_ToolBox {

    // Possible Connection Types on Left or Right of an Item(ToolBox/Connector)
    enum ToolBoxConnection{
        LEFT_TOOLBOX,
        RIGHT_TOOLBOX,
    };

    //  Connector states.
    //  Disconnected, Connection in progress, Connected, Failed
    enum ConnectionState{
        DISCONNECTED,
        IN_PROGRESS,
        CONNECTED,
        FAILED
    };
}


#endif // ME_TOOLBOXDEFS_H
