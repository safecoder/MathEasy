#ifndef ME_ICONNECTOR_H
#define ME_ICONNECTOR_H

#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <ToolBox/toolbox_global.h>
#include <ToolBox/ToolBoxDefs.h>

namespace ME_ToolBox {

    // Forward Declaration
    class IToolBox;

    class TOOLBOXSHARED_EXPORT IConnector
    {

    public:
        virtual ~IConnector(){}

        /// \brief  Initialize this connector
        virtual void initialize(QQmlApplicationEngine* engine) = 0;

        /// \brief  Get QML Item associated with it
        virtual QQuickItem* getQMLItem() = 0;

        /// \brief  Get the unique Id generated when this Item was created
        virtual const QString& getUniqueId() const = 0;

        /// \brief  Set Tool Box's which are connected either left or right
        virtual void setToolBox(QSharedPointer<IToolBox> tb, const ToolBoxConnection& connection) = 0;

        /// \brief  Unsets Tool Box's which are connected either left or right
        virtual void unsetToolBox(const ToolBoxConnection& connection) = 0;

        /// \brief  Sets the connection state, so that connector UI gets
        ///         Its shape and color w.r.t the sate
        virtual void setConnectorState(const ConnectionState& state) = 0;

        /// \brief  Resets connection
        virtual void reset() = 0;

        /// \brief  Deleter for Connector
        inline static void TOOLBOXSHARED_EXPORT doDeleteLater(ME_ToolBox::IConnector *obj)
        {
            QObject* qobj = dynamic_cast<QObject*>(obj);
            qobj->deleteLater();
        }
    };
}


#endif // ME_ICONNECTOR_H
