#ifndef ME_ITOOLBOX_H
#define ME_ITOOLBOX_H

#include <QQuickItem>
#include <QQmlApplicationEngine>
#include <ToolBox/toolbox_global.h>
#include <ToolBox/ToolBoxDefs.h>

namespace ME_ToolBox {

    // Forward Declaration
    class IConnector;

    class TOOLBOXSHARED_EXPORT IToolBox
    {

    public:
        virtual ~IToolBox(){}

        /// \brief  Initialize this tool box
        virtual void initialize(QQmlApplicationEngine* engine, QQuickItem* parent, int x=0, int y=0) = 0;

        /// \brief  Get QML Item associated with it
        virtual QQuickItem* getQMLItem() = 0;

        /// \brief  setter for unique name
        virtual void setUniqueName(const QString& uname) = 0;

        /// \brief  getter for unique name
        virtual const QString& getUniqueName() const = 0;

        /// \brief  setter for Tool Box Type
        virtual void setToolBoxType(const QString& type) = 0;

        /// \brief  getter for Tool Box Type
        virtual const QString& getToolBoxType() const = 0;

        /// \brief  Does this tool box has input pin
        virtual bool hasInputPin() = 0;

        /// \brief  Does this tool box has output pin
        virtual bool hasOutputPin() = 0;

        /// \brief  Set Title for this Tool Box
        virtual void setTitle(const QString& title) = 0;

        /// \brief  Add a Connector to the output pin of this Tool Box
        virtual bool addConnectorToOutputPin(IConnector* connector) = 0;

        /// \brief  Set Tool Box's which are connected either left or right
        virtual void setToolBox(QSharedPointer<IToolBox> tb, const ToolBoxConnection& connection) = 0;

        /// \brief  Unsets Tool Box's which are connected either left or right
        virtual void unsetToolBox(const ToolBoxConnection& connection) = 0;

        //Deleter for Tool Box
        inline static void TOOLBOXSHARED_EXPORT doDeleteLater(ME_ToolBox::IToolBox *obj)
        {
            QObject* qobj = dynamic_cast<QObject*>(obj);
            qobj->deleteLater();
        }
    };
}


#endif // ME_ITOOLBOX_H
