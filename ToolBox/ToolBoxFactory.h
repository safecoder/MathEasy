#ifndef ME_TOOLBOXFACTORY_H
#define ME_TOOLBOXFACTORY_H

#include <ToolBox/toolbox_global.h>
#include <QObject>

namespace ME_ToolBox {

    enum ObjType{
        ToolBoxType,
        ConnectorType
    };

    class TOOLBOXSHARED_EXPORT ToolBoxFactory
    {

    public:
        ToolBoxFactory(){}

        /// \brief  Create and return specified type Objects in this library
        QObject* createObj(const ObjType& type);
    };
}


#endif // ME_TOOLBOXFACTORY_H
