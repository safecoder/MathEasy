BUILD_PATH = ../../Build_MathEasy

# message(New build directory: $$BUILD_PATH)

Release:DESTDIR = $$BUILD_PATH/release
Release:OBJECTS_DIR = $$BUILD_PATH/release/.obj
Release:MOC_DIR = $$BUILD_PATH/release/.moc
Release:RCC_DIR = $$BUILD_PATH/release/.rcc
Release:UI_DIR = $$BUILD_PATH/release/.ui

Debug:DESTDIR = $$BUILD_PATH/debug
Debug:OBJECTS_DIR = $$BUILD_PATH/debug/.obj
Debug:MOC_DIR = $$BUILD_PATH/debug/.moc
Debug:RCC_DIR = $$BUILD_PATH/debug/.rcc
Debug:UI_DIR = $$BUILD_PATH/debug/.ui
