import QtQuick 2.4

Rectangle {
    id: buttonPanelArea
    width: parent===null ? 640 : parent.width;
    height: parent===null ? 80 : parent.height;
    color: "transparent"

    LayoutsPanel{
        visible: layoutSelectBtn.selected
        anchors.bottom: buttonPanelArea.top
    }

    Row{
        id: buttonRow
        spacing: 0

        ToolButton{
            id: layoutSelectBtn
            height: buttonPanelArea.height
            buttonLabel: "Lyts"
            selectOnClick: true
            onSelectedChanged: {
                if(selected){

                }
            }
        }

        ToolButton{
            id: mathFunction
            height: buttonPanelArea.height
            buttonLabel: "+Fnc"
            isDraggable: true
            toolName: "MathFunctions"
        }
    }
}





