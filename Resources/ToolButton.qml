import QtQuick 2.4

Rectangle {
    id: textButtonArea
    width: label.width + 20;
    height: 50;
    radius: 0
    color: normalColor

    border.width: 1
    border.color: Qt.lighter(normalColor, 1.5)

    // Signal that handles button click
    signal buttonClick()

    // Button customization parameters
    property bool selected: false
    property bool enableHover: false
    property bool selectOnClick: false
    property color normalColor: "#888"
    property color hoverColor: Qt.darker(normalColor, 1.5)
    property color selectedColor: Qt.darker(normalColor, 1.5)
    property string buttonLabel: "button"
    property string toolName: "ToolBox"
    property bool isDraggable: false

    Text{
        id: label
        text: buttonLabel
        color: "#CCC"
        font.family: "Courier"
        font.pixelSize: textButtonArea.height*0.7
        anchors.centerIn: textButtonArea
    }

    Rectangle{
        id: dragBtn
        width: parent.width
        height: parent.height
        radius: parent.radius
        color: selectedColor;
        border.width: selected ? 2 : 1;
        border.color: Qt.lighter(normalColor, 1.5)

        property alias toolName: textButtonArea.toolName

        Text{
            text: label.text
            color: label.color
            font.family: label.font.family
            font.pixelSize: label.font.pixelSize
            anchors.centerIn: dragBtn
        }

        property string dragItemKey : "dragBtn123"

        Drag.keys: [ dragItemKey ]
        Drag.active: buttonMouseArea.drag.active
        Drag.hotSpot.x: textButtonArea.width/2
        Drag.hotSpot.y: textButtonArea.height/2
        Drag.supportedActions: Qt.CopyAction

        function restorePosition() {
            dragBtn.x = 0
            dragBtn.y = 0
        }
    }

    MouseArea{
        id: buttonMouseArea
        anchors.fill: parent
        cursorShape: "PointingHandCursor"
        hoverEnabled: enableHover
        onClicked: {
            if(selectOnClick){
                selected = 1 - selected;
            }

            buttonClick();
        }

        drag.target: isDraggable ? dragBtn: null

        drag.onActiveChanged: {
            if (!buttonMouseArea.drag.active)
            {
                var dragResult = dragBtn.Drag.drop();
                if(dragResult === Qt.IgnoreAction)
                {
                        console.log("Drop ignored. Item must be restored.");
                        dragBtn.restorePosition()
                }
            }
        }
    }
}
