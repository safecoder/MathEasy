import QtQuick 2.4

// Visually a "Connection pipe" from/to a tool box
// Not just a GUI element, it handles little logics too
Canvas{
    id: canvas
    anchors.fill: parent
    objectName: "Connector"

    // No need to be visible if it didn't get a Tool Box
    visible: lToolBox !== null

    // Unique Id for identifying this connector from other connectors
    property string uniqueId: "noID"

    // Tool Box Connected to Left of this connector
    property ToolBoxWrapper lToolBox: null

    // Tool Box Connected to Right of this connector
    property ToolBoxWrapper rToolBox: null

    // Alias to the end points of the connector
    property alias startPoint: startPoint
    property alias endPoint: endPoint

    property int startX: startPoint.x + startPoint.width/2
    property int startY: startPoint.y + startPoint.height/2
    property int targetX: endPoint.x + endPoint.width/2
    property int targetY: endPoint.y + endPoint.height/2

    property string connectorState: "disconnected"
    property bool dragging: false
    property bool connected: false

    // Stroke color indicates the connection status
    property color strokeColor: {
        if(endPtMouseArea.drag.active)
            return "#DDA";

        if(connectorState==="disconnected")
            return "#EAA";
        else if(connectorState==="connected")
            return "#AEA";
        else if(connectorState==="failed")
            return "#F88";
    }

    // Signal for establishing a connection between lToolBox and rToolBox
    signal establishConnection(string connectorId, string ltoolBoxId, string rtoolBoxId);

    // Signal for resetting the connection
    signal resetConnection(int rToolBoxExists);

    // Signal send down to its adapter
    signal closeSignal();

    // Sets the connector state
    function changeConnectorState(state)
    {
        console.log("Connection changed to: " + state)
        connectorState = state;
    }

    // Remove this Connector
    function remove(){
        var rBoxExists = rToolBox===null ? 0: 1
        // send signal to reset the connection
        resetConnection(rBoxExists);
        console.log(uniqueId + ": connection reset");

        closeSignal();
    }

    // As soon as the connector is created, generate an Id for it.
    Component.onCompleted: {
        uniqueId = createUniqueId();
    }

    // Add an existing Tool Box to the Left of this connector
    // Now both start and end points are bound to Tool Box's output pin
    function addLToolBox(toolBoxObj) {
        lToolBox = toolBoxObj;
        console.log(uniqueId + ": lToolBox Added");
        startPoint.lBind();
        endPoint.lBind();
    }

    // Add an existing Tool Box to the Right of this connector
    // Now end point is bound to Tool Box's input pin
    function addRToolBox(toolBoxObj) {
        rToolBox = toolBoxObj;
        console.log(uniqueId + ": rToolBox Added");
        endPoint.rBind();
    }

    // Create and return a unique key for this connector
    function createUniqueId()
    {
        // Get a unique number
        var currDate = new Date().toLocaleString(Qt.locale(), "MMddHHmmsszzz");
        return "CN" + currDate;
    }

    // Resets the connector when the endpoint has been dropped anywhere
    // other than the input pin of a Tool Box
    function reset(){

        if(connected)
        {
            var rBoxExists = rToolBox===null ? 0: 1
            // send signal to reset the connection
            resetConnection(rBoxExists);
            console.log(uniqueId + ": connection reset");
        }

        endPoint.x = startPoint.x;
        endPoint.y = startPoint.y;
        endPoint.lBind();
        requestPaint();
        connected = false;
    }

    // Start Point of the connector pipe
    // Just a small disc with color
    Rectangle{
        id: startPoint
        width: 12
        height: width
        radius: width/2
        color: "#BBB"
        onXChanged: requestPaint()
        onYChanged: requestPaint()

        // Bind the position to the output pin of the Left Tool Box
        function lBind(){

                x = Qt.binding(
                        function(){
                            return lToolBox !== null ?
                                   lToolBox.x + lToolBox.outputPin.x :
                                    0;
                        }
                );

                y = Qt.binding(
                        function(){
                            return lToolBox !== null ?
                                   lToolBox.y + lToolBox.outputPin.y :
                                    0;
                        }
                );

        }
    }

    // End Point of the connector pipe
    // Just a small disc with color
    Rectangle{
        id: endPoint
        width: 8
        height: width
        radius: width/2
        color: endPtMouseArea.drag.active ? "#BEB" : "#EBB"
        onXChanged: requestPaint()
        onYChanged: requestPaint()

        // Get lToolBox Name to avoid self connection
        function getToolBoxName(){
            return lToolBox!==null ? lToolBox.uniqueName : "";
        }

        // Reset the connector
        function reset()
        {
            console.log("connector: endpt: reset")
            canvas.reset();
        }

        // Called when a connection is made successfully with an 'rToolBox'
        function connectionComplete(toolBoxObj)
        {
            addRToolBox(toolBoxObj);
            connected = true;

            // send signal to make the connection
            establishConnection(uniqueId, lToolBox.uniqueName, rToolBox.uniqueName);
            //console.log("connection complete");
        }

        // Bind the position to the output pin of the Left Tool Box
        function lBind(){

                x = Qt.binding(
                        function(){
                            return lToolBox !== null ?
                                   lToolBox.x + lToolBox.outputPin.x + lToolBox.outputPin.width/2 - width/2 :
                                    0
                        }
                );

                y = Qt.binding(
                        function(){
                            return lToolBox !== null ?
                                   lToolBox.y + lToolBox.outputPin.y + lToolBox.outputPin.height/2 - height/2 :
                                   0
                        }
                );
        }

        // Bind the position to the input pin of the Right Tool Box
        function rBind(){
                x = Qt.binding(
                        function(){
                            if(rToolBox.visible && rToolBox !== null){
                                   return rToolBox.x + rToolBox.inputPin.x + rToolBox.inputPin.width/2 - width/2;
                            }
                            else
                            {
                                canvas.reset();
                            }
                        }
                );

                y = Qt.binding(
                        function(){
                            if(rToolBox !== null){
                                   return rToolBox.y + rToolBox.inputPin.y + rToolBox.inputPin.height/2 - height/2;
                            }
                            else
                            {
                                canvas.reset();
                            }
                        }
                );
        }

        // Make it draggable untill we make a successful connection
        Drag.active: endPtMouseArea.drag.active
        Drag.keys: [ "dragConnectorPt123" ]
        Drag.hotSpot.x: endPoint.width/2
        Drag.hotSpot.y: endPoint.height/2

        MouseArea{
            id: endPtMouseArea
            anchors.fill: parent
            hoverEnabled: true

            drag.target: connected ? null : endPoint

            drag.onActiveChanged: {
                if (!endPtMouseArea.drag.active)
                {
                    var dragResult = endPoint.Drag.drop();
                    if(dragResult === Qt.IgnoreAction)
                    {
                            console.log("Connection is not made. reset connector.");
                            reset();
                    }
                }
            }
        }
    }

    // Draw the connector pipe, Call whenever we need an update of it.
    function drawLine(context){

        // Make canvas all white
        context.beginPath();
        context.clearRect(0, 0, width, height);
        context.fill();

        // Draw a line
        context.beginPath();
        context.lineWidth = 3;
        context.moveTo(startX, startY);
        context.strokeStyle = strokeColor
        context.lineTo(targetX, targetY);
        context.stroke();
    }

    onPaint: {
        // Get drawing context
        var context = getContext("2d");
        drawLine(context);
    }
}
