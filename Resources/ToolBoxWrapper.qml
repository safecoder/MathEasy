import QtQuick 2.4

// This is a wrapper for all ToolBox Items
// It takes care of resizing, scrolling, and other common functionalities
Rectangle {
    id: toolBoxWrapper
    objectName: "ToolBox"
    width: 320
    height: 240
    color: "#DCDCDC"
    border.color: Qt.darker(color, 2)
    border.width: 1

    // List of connectors connected to the output pin
    property var connectorList: []

    // Unque Name of this tool box
    property string uniqueName: "uniquename"

    // Title for the tool box
    property string title: "Tool Box"

    // Signal for sending the button click info
    signal buttonSignal(string buttonName, string value)

    property color favColor: "#445533"

    // minimum possible dimension for a ToolBox
    property int minWidth: 200
    property int minHeight: 200

    // Has input pin and/or output pin
    property bool hasLeftPin: false
    property bool hasRightPin: false

    // Alias to the i/o pins
    property alias outputPin: outputPin
    property alias inputPin: inputPin

    // Make it draggable across the PlayGround
    Drag.active: draggableArea.drag.active
    Drag.hotSpot.x: 0
    Drag.hotSpot.y: 0

    // Do at Startup
    onParentChanged: {
        if(parent !== null)
        {
            // Autosnap at startup
            //adjustPosition();
        }
    }

    // Need to be initialized
    onUniqueNameChanged: {
        if(uniqueName !== "uniquename" && parent !== null)
        {
            toolBoxWrapper.parent.addUniniatializedBox(toolBoxWrapper);
        }
    }

    // Autosnap to nearest boundaries
    function adjustPosition()
    {
        if(parent===null) return;

        parent.getTargetBoundaries();

        var x1TargetValue = parent.getNearestValue("x", toolBoxWrapper.x - 15);
        var x2TargetValue = parent.getNearestValue("x", toolBoxWrapper.x + toolBoxWrapper.width + 15);
        var y1TargetValue = parent.getNearestValue("y", toolBoxWrapper.y);
        var y2TargetValue = parent.getNearestValue("y", toolBoxWrapper.y + toolBoxWrapper.height);

        x1TargetValue = Math.abs(x1TargetValue) < Math.abs(x2TargetValue) ? x1TargetValue: x2TargetValue;
        y1TargetValue = Math.abs(y1TargetValue) < Math.abs(y2TargetValue) ? y1TargetValue: y2TargetValue;

        //console.log("Snap to: " + x1TargetValue + ", " + y1TargetValue)

        if(Math.abs(x1TargetValue) < 30)
        {
            toolBoxWrapper.x += x1TargetValue
        }

        if(Math.abs(y1TargetValue) < 15)
        {
            toolBoxWrapper.y += y1TargetValue
        }
    }

    //  Add A ConnectorPipe to the output Pin
    function addConnector(connectorId)
    {
        console.log("Trying to Add connector " + connectorId);
        var playgroundChildren = parent.children;
        for(var i=0; i<playgroundChildren.length; i++)
        {
            if(playgroundChildren[i].objectName==="Connector" &&
               playgroundChildren[i].uniqueId === connectorId)
            {
                playgroundChildren[i].addLToolBox(toolBoxWrapper)
                console.log("Added connector " + playgroundChildren[i].uniqueId +
                            " to the Tool Box " + toolBoxWrapper.uniqueName);
                connectorList.push(playgroundChildren[i]);
            }
        }
    }

    // Reset All Connectors
    function resetAllConnectors(){
        for(var i=0; i<connectorList.length; i++)
        {
            connectorList[i].reset();
        }
    }

    // Title Bar for the Tool Box Wrapper
    Rectangle{
        id: titleBar
        height: 15
        width: toolBoxWrapper.width
        color: Qt.darker(toolBoxWrapper.color, 2)
        clip: true
        anchors.top: toolBoxWrapper.top

        Text{
            text: title
            anchors.centerIn: parent
            color: "#EEE"
            font.pixelSize: titleBar.height*0.8
        }

        MouseArea{
            id: draggableArea
            anchors.fill: parent

            // Make ToolBox draggable inside playGround
            drag.target: toolBoxWrapper
            drag.axis: Drag.XAndYAxis
            drag.minimumX: 0
            drag.maximumX: toolBoxWrapper.parent !== null ?
                               toolBoxWrapper.parent.width - toolBoxWrapper.width : 0
            drag.minimumY: 0
            drag.maximumY: toolBoxWrapper.parent !== null ?
                               toolBoxWrapper.parent.height - toolBoxWrapper.height : 0

            drag.onActiveChanged: {
                if (!drag.active)
                {
                    //console.log("Drag Finished")
                    adjustPosition();
                }
            }
        }

        // Close button
        // Clicking this button will close ToolBox
        SimpleButton{
            id: closeBtn
            anchors.right: titleBar.right
            height: titleBar.height
            width: height
            buttonLabel: "X"
            normalColor: "transparent"
            labelColor: "#DDD"
            border.width: 0
            enableHover: true
            hoverColor: Qt.darker(toolBoxWrapper.color, 1.7)
            fontHeightFactor: 0.9

            // Send close signal
            onButtonClick: {

                toolBoxWrapper.visible = false;

                for(var i=0; i<connectorList.length; i++)
                {
                    if(connectorList[i] !== null)
                        connectorList[i].remove();
                }

                buttonSignal("close", "");
            }
        }
    }

    // The real content of the ToolBoxWrapper is under ScrollableArea
    // It has one of its child as content item
    ScrollableArea{
        anchors.top: titleBar.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }

    // This part of the code is for resizing the ToolBoxWrapper item
    // It can be resized within the playground considering a minimum size
    MouseArea{
        id: resizePoint
        width: 10
        height: 10
        x: toolBoxWrapper.width-width/2
        y: toolBoxWrapper.height-height/2
        cursorShape: Qt.SizeFDiagCursor
        hoverEnabled: true

        drag.minimumX: minWidth
        drag.maximumX: toolBoxWrapper.parent!==null ?
                       toolBoxWrapper.parent.width-toolBoxWrapper.x: minWidth
        drag.minimumY: minHeight
        drag.maximumY: toolBoxWrapper.parent!==null ?
                       toolBoxWrapper.parent.height-toolBoxWrapper.y: minHeight

        drag.target: resizePoint

        // draggable element inorder to do resize
        Drag.active: resizePoint.drag.active
        Drag.hotSpot.x: width/2
        Drag.hotSpot.y: height/2

        // Resize in x direction
        onXChanged: {
            var newPossibleWidth = x+width/2
            toolBoxWrapper.width = newPossibleWidth;
        }

        // Resize in y direction
        onYChanged: {
            var newPossibleHeight = y+height/2
            toolBoxWrapper.height = newPossibleHeight
        }

        // Visual Rectangle to indicate resizability
        Rectangle{
            id: resizeDragger
            width: 5
            height: 5
            anchors.centerIn: resizePoint
            color: "#555"
        }
    }

    // Input Pin
    PinDot{
        id: inputPin
        x: -width
        y: minHeight*0.66
        visible: hasLeftPin
        border.color: "#777"
        border.width: inputDrpArea.containsDrag ? 1: 0

        DropArea {
            id: inputDrpArea
            anchors.fill: parent
            property alias dropProxy: inputDrpArea

            keys: ["dragConnectorPt123"]

            onDropped: {
                // Avoid connection from the same Tool Box to itself
                if(drop.source.getToolBoxName() === toolBoxWrapper.uniqueName)
                {
                    return;
                }

                drop.acceptProposedAction()
                //console.log("Got a connection")
                drop.source.connectionComplete(toolBoxWrapper);
            }
        }
    }

    // Output Pin
    PinDot{
        id:outputPin
        x: toolBoxWrapper.width
        y: minHeight*0.66
        visible: hasRightPin
    }
}
