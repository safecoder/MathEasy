import QtQuick 2.4

Rectangle{
    height: 100
    width: buttonPanelArea.width
    border.color: "#888"
    border.width: 1
    focus: true

    onActiveFocusChanged: {
        // console.log("focus change to "+activeFocus)
    }

    ListModel{
        id: layoutModel
        ListElement{ layoutType:"grid"; layoutData:"2X1"}
        ListElement{ layoutType:"grid"; layoutData:"3X2"}
        ListElement{ layoutType:"grid"; layoutData:"3X3"}
        ListElement{ layoutType:"grid"; layoutData:"3X1"}
        ListElement{ layoutType:"grid"; layoutData:"2X2"}
        ListElement{ layoutType:"grid"; layoutData:"2X1"}
        ListElement{ layoutType:"grid"; layoutData:"3X3"}
        ListElement{ layoutType:"grid"; layoutData:"2X2"}
        ListElement{ layoutType:"grid"; layoutData:"2X2"}
        ListElement{ layoutType:"grid"; layoutData:"2X2"}
        ListElement{ layoutType:"grid"; layoutData:"2X2"}
    }

    ListView{
        id: layoutList
        anchors.left: leftScrollBtn.right
        anchors.right: rightScrollBtn.left
        height: parent.height*0.8
        anchors.verticalCenter: parent.verticalCenter
        spacing: 20
        orientation: ListView.Horizontal

        // Flick horizontally
        clip: true
        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.HorizontalFlick

        model: layoutModel

        delegate: Component{
            Rectangle{
                id: layoutButtonWrapper
                height: layoutList.height
                width: height

                LayoutDisplay{
                    anchors.fill: parent
                    type: layoutType
                    data: layoutData
                    Component.onCompleted: createLayout();
                    onWidthChanged: createLayout();
                    onHeightChanged: createLayout();
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        playGroundLayout.type = layoutType;
                        playGroundLayout.data = layoutData;
                        playGroundLayout.createLayout();
                    }
                }
            }
        }
    }

    ToolButton{
        id: leftScrollBtn
        width: 0.05*parent.width
        height: parent.height
        color: "#AAA"
        buttonLabel: "<"
        anchors.left: parent.left
        onButtonClick: layoutList.flick(-800, 0);
    }

    ToolButton{
        id: rightScrollBtn
        width: 0.05*parent.width
        height: parent.height
        color: "#AAA"
        buttonLabel: ">"
        anchors.right: parent.right
        onButtonClick: layoutList.flick(800, 0);
    }
}
