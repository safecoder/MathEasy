import QtQuick 2.4

Rectangle{
    id: layoutDisplay
    height: 100
    width: 100
    border.width: 1
    border.color: "#777"
    color: "#EEE"

    property string type: "grid"
    property string data: "2X1"

    function createLayout(){
        // First we clear all previously written
        clearLayout();

        if(type=="grid")
        {
            var gridDimensions = data.split("X");
            var rows = parseInt(gridDimensions[0]);
            var columns = parseInt(gridDimensions[1]);
            var td = 0.02
            var cellWidth = (1.0-td)/columns;
            var cellHeight = (1.0-td)/rows;


            for(var i=0; i<rows; i++)
            {
                for(var j=0; j<columns; j++)
                {
                    drawReactangle(j*cellWidth + td, i*cellHeight + td, cellWidth-td, cellHeight-td);
                }
            }
        }
        else if(type=="custom")
        {

        }
    }

    function clearLayout(){
        var childrenRects = layoutDisplay.children;
        for(var i=childrenRects.length-1; i>=0; i--){
            if(childrenRects[i].objectName=="innerRect")
                childrenRects[i].destroy();
        }
    }

    function drawReactangle(xFac, yFac, wFac, hFac){
        var newObject = Qt.createQmlObject('import QtQuick 2.4; Rectangle {}',
            layoutDisplay, "./");
        newObject.x = layoutDisplay.width*xFac;
        newObject.y = layoutDisplay.height*yFac;
        newObject.width = layoutDisplay.width*wFac;
        newObject.height = layoutDisplay.height*hFac;
        newObject.color = "transparent"
        newObject.border.color = Qt.darker(layoutDisplay.color, 1.3)
        newObject.border.width = 1;
        newObject.objectName = "innerRect"
    }
}
