import QtQuick 2.4

Rectangle {
    id: scrollableArea
    width: 200
    height: 200
    color: "transparent"
    border.color: "#999"
    border.width: 1

    property color scrollBarColor: "#AAA"
    property int scrollBarWidth: 15

    Rectangle{
        id: verticalScroller
        color: Qt.lighter(scrollBarColor, 1.2)
        opacity: 0.8
        width: scrollBarWidth
        height: scrollableArea.height-scrollBarWidth
        anchors.right: scrollableArea.right
        visible: flickableArea.contentHeight > flickableArea.height

        Rectangle{
            id: vertScrollBar
            width: parent.width
            height: (flickableArea.height/flickableArea.contentHeight)*parent.height
            color: scrollBarColor

            Drag.active: vertScrollBarMouseArea.drag.active
            Drag.hotSpot.y: 0

            onYChanged: {
                if(vertScrollBarMouseArea.drag.active)
                    flickableArea.contentY = (y / verticalScroller.height)*flickableArea.contentHeight;
            }

            MouseArea{
                id: vertScrollBarMouseArea
                anchors.fill: parent

                drag.target: vertScrollBar
                drag.axis: Drag.YAxis
                drag.minimumY: 0
                drag.maximumY: verticalScroller.height - vertScrollBar.height
            }
        }
    }

    Rectangle{
        id: horizontalScroller
        color: verticalScroller.color
        opacity: verticalScroller.opacity
        width: scrollableArea.width-scrollBarWidth
        height: scrollBarWidth
        anchors.bottom: scrollableArea.bottom
        visible: flickableArea.contentWidth > flickableArea.width

        Rectangle{
            id: horizScrollBar
            width: (flickableArea.width/flickableArea.contentWidth)*parent.width
            height: parent.height
            color: scrollBarColor

            Drag.active: horizScrollBarMouseArea.drag.active
            Drag.hotSpot.x: 0

            onXChanged: {
                if(horizScrollBarMouseArea.drag.active)
                    flickableArea.contentX = (x / horizontalScroller.width)*flickableArea.contentWidth;
            }

            MouseArea{
                id: horizScrollBarMouseArea
                anchors.fill: parent

                drag.target: horizScrollBar
                drag.axis: Drag.XAxis
                drag.minimumX: 0
                drag.maximumX: horizontalScroller.width - horizScrollBar.width
            }
        }
    }

    Flickable{
        id: flickableArea
        objectName: "flickableArea"
        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.HorizontalAndVerticalFlick
        clip: true

        anchors.left: scrollableArea.left
        anchors.top: scrollableArea.top
        anchors.right: verticalScroller.left
        anchors.bottom: horizontalScroller.top

        contentX: 0
        contentY: 0
        contentWidth: contentArea.width
        contentHeight: contentArea.height

        onContentYChanged: {
            if(!vertScrollBarMouseArea.drag.active){
                vertScrollBar.y = contentY/contentHeight*verticalScroller.height
            }
        }

        onContentXChanged: {
            if(!horizScrollBarMouseArea.drag.active){
                horizScrollBar.x = contentX/contentWidth*horizontalScroller.width
            }
        }

        Rectangle{
            id: contentArea
            objectName: "contentArea"
            width: 500
            height: 300
            color: "transparent"
        }
    }
}
