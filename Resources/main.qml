import QtQuick 2.4

Rectangle {
    id: wrapper
    width: parent===null ? 640 : parent.width;
    height: parent===null ? 480 : parent.height;
    color: "#DDD"

    // Signal for sending the button click info
    signal buttonSignal(string buttonName)

    Column{
        width: parent.width
        spacing: 5

        Rectangle{
            id: titleBar
            width: wrapper.width
            height: 20
            color: "#AAA"

            Text{
                text: applicationName
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.margins: 10
                font.pixelSize: parent.height*0.8
                color: "white"
            }
        }

        PlayGround{
            id: playGround
            objectName: "playGround"
            width: wrapper.width-20
            height: wrapper.height-titleBar.height-taskBar.height-10
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Rectangle{
            id: taskBar
            width: wrapper.width
            height: 50;
            color: "#999"

            ButtonPanel{

            }
        }
    }
}
