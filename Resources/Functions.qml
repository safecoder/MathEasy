import QtQuick 2.4

Rectangle {
    width: 400
    height: 300
    color: "#BCCCBC"

    // Signal for sending the button click info
    signal buttonSignal(string buttonName)

    property color favColor: "#445533"

    Column{
        width: parent.width
        anchors.top: parent.top
        anchors.margins: parent.width*0.01
        spacing: 20

        Text{
            id: label
            text: "Add new function"
            color: favColor
            font.pixelSize: 15
            anchors.horizontalCenter: parent.horizontalCenter
        }

        TextInputBox{
            id: textInputBox
            width: 200
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
