import QtQuick 2.4

// Connection Pins for input or output from/to a tool box
Rectangle{
    id: dot
    width: 12
    height: width
    radius: width/2
    color: "#BBB"

    signal connectionStart()
    signal connectionEnd()
}
