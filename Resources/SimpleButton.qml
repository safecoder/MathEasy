import QtQuick 2.4

Rectangle {
    id: simpleButton
    width: label.width + 20;
    height: 50;
    radius: 0
    color: buttonMouseArea.containsMouse ? hoverColor: normalColor

    border.width: 1
    border.color: Qt.lighter(normalColor, 1.5)

    // Signal that handles button click
    signal buttonClick()

    // Button customization parameters
    property bool selected: false
    property bool enableHover: false
    property bool selectOnClick: false
    property color normalColor: "#888"
    property color hoverColor: Qt.darker(normalColor, 1.5)
    property color selectedColor: Qt.darker(normalColor, 1.5)
    property color labelColor: "#CCC"
    property string buttonLabel: "button"
    property string toolName: "ToolBox"
    property bool isDraggable: false
    property real fontHeightFactor: 0.7

    Text{
        id: label
        text: buttonLabel
        color: labelColor
        font.family: "Courier"
        font.pixelSize: simpleButton.height*fontHeightFactor
        anchors.centerIn: simpleButton
    }

    MouseArea{
        id: buttonMouseArea
        anchors.fill: parent
        cursorShape: "PointingHandCursor"
        hoverEnabled: enableHover
        onClicked: {
            if(selectOnClick){
                selected = 1 - selected;
            }

            buttonClick();
        }
    }
}
