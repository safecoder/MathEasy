import QtQuick 2.4

//  The Main Area exposed to end users to visualize tool box creation
//  and connectivites.
Rectangle{
    id: playGround
    objectName: "playGround"
    anchors.horizontalCenter: parent.horizontalCenter
    border.color: "#999"
    border.width: 1
    radius: 5
    color: "#EEE"

    // List of tool boxes needed initialization
    property var uninitializedBoxes: []

    // Signal for creating a tool box at specified position
    signal createToolBox(string toolName, int x, int y);

    // Signal for creating and adding a new connector to a Tool Box
    signal createAndAddConnector(string toolBoxName);

    // The list of target boundaries to which tool boxes can snap
    property var magnetEdges: {"x":[], "y":[] }

    Component.onCompleted: {
        appWatchMan.start();
    }

    function addUniniatializedBox(boxObj)
    {
        uninitializedBoxes.push(boxObj)
    }

    LayoutDisplay{
        id: playGroundLayout
        anchors.fill: parent
        type: "grid"
        data: "1X1"
        Component.onCompleted: createLayout()
        onWidthChanged: createLayout()
        onHeightChanged: createLayout()
        visible: false
    }

    //  Get nearest value for displacement, given the axis
    //  and the value to be compared
    function getNearestValue(axis, currentValue)
    {
        //console.log("Getting nearest value for: " + currentValue)

        var minimumDisplacement = 100;
        var targetList = axis==="x" ? magnetEdges.x : magnetEdges.y;
        for(var i=0; i< targetList.length; i++)
        {
            var displacement = targetList[i]-currentValue;
            if(Math.abs(displacement) > 0 && Math.abs(displacement) < Math.abs(minimumDisplacement))
            {
                //console.log("Updated min value as: " + minimumDisplacement)
                minimumDisplacement = displacement;
            }
        }
        return minimumDisplacement;
    }

    // Update a list of magnet edges that tool boxes can snap to
    function getTargetBoundaries(){

        magnetEdges = {"x":[], "y":[] }
        magnetEdges.x.push(0);
        magnetEdges.x.push(playGround.width);
        magnetEdges.y.push(0);
        magnetEdges.y.push(playGround.height);

        var children = playGround.children;
        for(var i=0; i< children.length; i++)
        {
            if(children[i].objectName === "ToolBox")
            {
                magnetEdges.x.push(children[i].x - 15);
                magnetEdges.x.push(children[i].x + children[i].width + 15);
                magnetEdges.y.push(children[i].y);
                magnetEdges.y.push(children[i].y + children[i].height);
            }
        }
        console.log(JSON.stringify(magnetEdges));
    }

    // This is the main drop area for triggering new tool box addition
    // Tool Buttons are drag-n-dropped into this area, which emits a signal
    // to add the corresponding Tool Box at that position.
    DropArea {
        anchors.fill: parent
        id: dragTarget
        objectName: "playgroundDropArea"

        property alias dropProxy: dragTarget

        keys: ["dragBtn123", "dragItemAbc"]

        Rectangle {
            anchors.fill: parent
            color: Qt.darker(playGround.color, 1.2)

            visible: parent.containsDrag
        }

        onDropped: {
            drop.acceptProposedAction()
            drop.source.restorePosition();

            // Get a unique number
            var currDate = new Date().toLocaleString(Qt.locale(), "MMddHHmmsszzz");
            var newToolBoxName = drop.source.toolName + "_TB" + currDate
            createToolBox(newToolBoxName, drop.x, drop.y)

            console.log("dropped at: "+ drop.x + "," + drop.y + "," + drop.source.toolName)
        }
    }


    // A timer for watching the App
    Timer{
        id: appWatchMan
        interval: 1000
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            // JOB

            //console.log("triggered")
            for(var i=0; i<uninitializedBoxes.length; i++)
            {
                if(uninitializedBoxes[i]!==null)
                {
                    createAndAddConnector(uninitializedBoxes[i].uniqueName);
                }
            }
            uninitializedBoxes = [];
        }
    }
}
