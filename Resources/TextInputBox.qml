import QtQuick 2.4

Rectangle {
    id: inputBoxWrapper
    width: 150;
    height: 25
    color: "#EEE"
    radius: 4
    border.width: 1
    border.color: "#999"
    clip: true

    property alias textInput: textInput
    property alias text: textInput.text

    TextInput{
        id: textInput
        text: ""
        color: "#666"
        maximumLength: 40
        anchors.centerIn: parent
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 4
        font.pixelSize: inputBoxWrapper.height*0.7
    }

    MouseArea{
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.IBeamCursor
        onClicked: {
            textInput.forceActiveFocus();
        }
    }
}
