
#include <MathFunction/MathFunctionTool.h>
#include <METoolFactory/METoolGallery.h>

namespace ME_ToolFactory
{
    METoolGallery::METoolGallery()
    {
    }

    ME_ToolBox::IToolBox*
    METoolGallery::createToolFromType(const QString& toolType)
    {
        if(toolType=="MathFunctions")
        {
            //return 0;
            return (ME_ToolBox::IToolBox*)(new ME_Functions::MathFunction(0));
        }
        return NULL;
    }
}
