#-------------------------------------------------
#
# Project created by QtCreator 2015-06-08T00:30:40
#
#-------------------------------------------------

# build setup
include(../Config/build.pri)

QT       -= gui
QT += qml quick

TARGET = METoolFactory
TEMPLATE = lib

DEFINES += METOOLFACTORY_LIBRARY

SOURCES += METoolFactory.cpp \
    METoolGallery.cpp

HEADERS += METoolFactory.h\
        metoolfactory_global.h \
    METoolGallery.h \
    IMEToolGallery.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$BUILD_PATH/release/ -lMathFunction
else:win32:CONFIG(debug, debug|release): LIBS += -L$$BUILD_PATH/debug/ -lMathFunction
else:unix: LIBS += -L$$BUILD_PATH/ -lMathFunction

DEPENDPATH += $$PWD/../MathFunction
INCLUDEPATH += $$PWD/..

