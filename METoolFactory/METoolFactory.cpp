#include <METoolFactory/METoolFactory.h>
#include <METoolFactory/METoolGallery.h>

namespace ME_ToolFactory
{
    METoolFactory::METoolFactory()
    {
        _toolGallery = QSharedPointer<IMEToolGallery>(new METoolGallery());
    }

    ME_ToolBox::IToolBox*
    METoolFactory::createToolObj(const QString& toolType)
    {
        return _toolGallery->createToolFromType(toolType);
    }
}
