#ifndef IMETOOLGALLERY_H
#define IMETOOLGALLERY_H

#include <QObject>

namespace ME_ToolBox
{
    class IToolBox;
}

namespace ME_ToolFactory
{
    class IMEToolGallery
    {

    public:
        virtual ~IMEToolGallery(){}

        /// \brief  Create and return specified type Objects in this library
        virtual ME_ToolBox::IToolBox* createToolFromType(const QString& toolType) = 0;
    };
}
#endif // IMETOOLGALLERY_H
