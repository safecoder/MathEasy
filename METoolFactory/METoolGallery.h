#ifndef METOOLGALLERY_H
#define METOOLGALLERY_H

#include <QObject>
#include <METoolFactory/IMEToolGallery.h>

namespace ME_ToolFactory
{
    class METoolGallery: public IMEToolGallery
    {

    public:
        METoolGallery();

        /// \brief  Create and return specified type Objects in this library
        ME_ToolBox::IToolBox* createToolFromType(const QString& toolType);
    };
}

#endif // METOOLGALLERY_H
