#ifndef METOOLFACTORY_H
#define METOOLFACTORY_H

#include <METoolFactory/metoolfactory_global.h>
#include <METoolFactory/IMEToolGallery.h>
#include <QSharedPointer>
#include <QObject>

namespace ME_ToolFactory
{
    class METOOLFACTORYSHARED_EXPORT METoolFactory
    {

    public:
        METoolFactory();

        /// \brief  Create and return specified type Objects in this library
        QObject* createToolObj(const QString& toolType);

    private:

        QSharedPointer<IMEToolGallery> _toolGallery;
    };
}
#endif // METOOLFACTORY_H
