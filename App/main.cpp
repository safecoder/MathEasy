#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <assert.h>
#include <QSharedPointer>

#include "RootAdapter.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    // Set the root view for the application
    QQuickView* rootView = new QQuickView();
    rootView->setResizeMode(QQuickView::SizeRootObjectToView);
    rootView->setSource(QUrl("qrc:///main.qml"));

    MEApp::IRootAdapter* rootAdptr = new MEApp::RootAdapter();
    QSharedPointer<MEApp::IRootAdapter> rootAdptrPtr =
            QSharedPointer<MEApp::IRootAdapter>(rootAdptr);

    // Initialize the base Adapter
    rootAdptrPtr->initialize(&engine, rootView);

    // Show up the window of the Application
    rootAdptrPtr->showApp();

    return app.exec();
}
