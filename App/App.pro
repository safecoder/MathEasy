# build setup
include(../Config/build.pri)

TEMPLATE = app

QT += qml quick

SOURCES += main.cpp \
    RootAdapter.cpp \
    MEAdapter.cpp \
    LayoutAdapter.cpp

HEADERS += \
    RootAdapter.h \
    IRootAdapter.h \
    Const.h \
    IMEAdapter.h \
    MEAdapter.h \
    ILayoutAdapter.h \
    LayoutAdapter.h

RESOURCES += ../Resources/resources.qrc
RC_FILE = ../Resources/rc/App.rc

INCLUDEPATH += $$PWD/..

# Default rules for deployment.
include(deployment.pri)

# message($$OUT_PWD)

win32:CONFIG(release, debug|release): LIBS += -L$$BUILD_PATH/release/ -lLogicLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$BUILD_PATH/debug/ -lLogicLib
else:unix: LIBS += -L$$BUILD_PATH/ -lLogicLib

DEPENDPATH += $$PWD/../LogicLib

win32:CONFIG(release, debug|release): LIBS += -L$$BUILD_PATH/release/ -lToolBox
else:win32:CONFIG(debug, debug|release): LIBS += -L$$BUILD_PATH/debug/ -lToolBox
else:unix: LIBS += -L$$BUILD_PATH/ -lToolBox

DEPENDPATH += $$PWD/../ToolBox

win32:CONFIG(release, debug|release): LIBS += -L$$BUILD_PATH/release/ -lMEToolFactory
else:win32:CONFIG(debug, debug|release): LIBS += -L$$BUILD_PATH/debug/ -lMEToolFactory
else:unix: LIBS += -L$$BUILD_PATH/ -lMEToolFactory

DEPENDPATH += $$PWD/../METoolFactory

win32:CONFIG(release, debug|release): LIBS += -L$$BUILD_PATH/release/ -lMathFunction
else:win32:CONFIG(debug, debug|release): LIBS += -L$$BUILD_PATH/debug/ -lMathFunction
else:unix: LIBS += -L$$BUILD_PATH/ -lMathFunction

DEPENDPATH += $$PWD/../MathFunction

