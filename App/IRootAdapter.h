#ifndef ME_IROOT_ADAPTER_
#define ME_IROOT_ADAPTER_

#include <QQmlApplicationEngine>
#include <QQuickView>

namespace MEApp
{
    class IRootAdapter{

    public:
        virtual ~IRootAdapter(){}

        /// \brief  Initialize this adapter
        virtual void initialize(QQmlApplicationEngine* engine, QQuickView* root) = 0;

        /// \brief  Show the application window
        virtual void showApp() = 0;
    };
}


#endif // ME_IROOT_ADAPTER_
