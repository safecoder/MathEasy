#include <QScreen>
#include <assert.h>

#include "RootAdapter.h"
#include "Const.h"

#include <LogicLib/ILogicManager.h>
#include <LogicLib/LogicLibFactory.h>

#include <METoolFactory/METoolFactory.h>
#include <ToolBox/ToolBoxFactory.h>
#include <ToolBox/IToolBox.h>
#include <ToolBox/IConnector.h>

namespace MEApp
{
    RootAdapter::RootAdapter(QObject* parent):
    QObject(parent)
    {
    }

    RootAdapter::~RootAdapter()
    {
        qDebug() << "RootAdapter Dtor";
    }

    void
    RootAdapter::initialize(QQmlApplicationEngine* engine, QQuickView* root)
    {
        MEAdapter::initialize(engine, root);

        QObject* rootObj = _rootView->rootObject();
        _playGroundItem = rootObj->findChild<QQuickItem*>("playGround");
        assert(_playGroundItem);

        // Create necessary factory instances
        _meToolFactory = QSharedPointer<ME_ToolFactory::METoolFactory>(new ME_ToolFactory::METoolFactory());
        _toolBoxFactory = QSharedPointer<ME_ToolBox::ToolBoxFactory>(new ME_ToolBox::ToolBoxFactory());

        bool connected;
        connected = connect((QObject*)_rootView->rootObject(),
                            SIGNAL(buttonSignal(QString)),
                            this,
                            SLOT(_handleButtonClick(QString)));

        assert(connected);

        connected = connect((QObject*)_playGroundItem,
                            SIGNAL(createToolBox(QString, int, int)),
                            this,
                            SLOT(_handleToolBoxCreation(QString, int, int)));

        assert(connected);

        connected = connect((QObject*)_playGroundItem,
                            SIGNAL(createAndAddConnector(QString)),
                            this,
                            SLOT(_handleNewConnectorAddition(QString)));

        assert(connected);
    }

    void
    RootAdapter::_handleNewConnectorAddition(const QString& toolBoxName)
    {
        ToolBoxIterator itr;
        itr = _toolBoxMap.find(toolBoxName);

        if(itr != _toolBoxMap.end())
        {
            // Add a Connector only if it has an output pin
            if(itr.value()->hasOutputPin())
            {
                // Create a new Connector inside the Playground
                ME_ToolBox::IConnector* connector = _addNewConnectorToPlayGround();

                // Set the left Tool Box of the Connector as this Tool Box Obj
                connector->setToolBox(itr.value(), ME_ToolBox::LEFT_TOOLBOX);

                //  Add newly created Connector to the Output pin of the ToolBox
                itr.value()->addConnectorToOutputPin(connector);
            }
        }
    }

    void
    RootAdapter::_handleToolBoxCreation(QString name, int x, int y)
    {
        QStringList splitData = name.split("_");
        QString toolType = splitData[0];

        ME_ToolBox::IToolBox* obj = _meToolFactory->createToolObj(toolType);
        assert(obj);

        //Initialize it soon
        assert(_playGroundItem);
        obj->initialize(_engine, _playGroundItem, x, y);

        _toolBoxMap[name] = QSharedPointer<ME_ToolBox::IToolBox>(obj, &ME_ToolBox::IToolBox::doDeleteLater);
        _toolBoxMap[name]->setUniqueName(name);
        _toolBoxMap[name]->setToolBoxType(toolType);

        // Connect to the signals
        bool connected = connect(dynamic_cast<QObject*>(_toolBoxMap[name].data()),
                                 SIGNAL(closeToolBoxSignal(QString)),
                                 this,
                                 SLOT(_handleToolBoxClosing(QString)));

        assert(connected);
    }

    ME_ToolBox::IConnector*
    RootAdapter::_addNewConnectorToPlayGround()
    {
        // Create a new connector obj, initialize it.
        ME_ToolBox::IConnector* connector = dynamic_cast<ME_ToolBox::IConnector*>(_toolBoxFactory->createObj(ME_ToolBox::ConnectorType));
        assert(connector);

        connector->initialize(_engine);
        QString connectorId = connector->getUniqueId();
        assert(connectorId != "");

        _connectorMap[connectorId] = QSharedPointer<ME_ToolBox::IConnector>(connector, &ME_ToolBox::IConnector::doDeleteLater);

        // Add to the playGround Item
        assert(_playGroundItem);
        _connectorMap[connectorId]->getQMLItem()->setParentItem(_playGroundItem);

        // Connect to the signals
        bool connected = connect(dynamic_cast<QObject*>(_connectorMap[connectorId].data()),
                                 SIGNAL(remove(QString)),
                                 this,
                                 SLOT(_destroyConnector(QString)));

        assert(connected);

        connected = connect(dynamic_cast<QObject*>(_connectorMap[connectorId].data()->getQMLItem()),
                             SIGNAL(establishConnection(QString, QString, QString)),
                             this,
                             SLOT(_handleConnectionEstablishment(QString, QString, QString)));

        assert(connected);

        return connector;
    }

    void
    RootAdapter::_handleConnectionEstablishment(const QString& connectorId, const QString& lToolBoxId, const QString& rToolBoxId)
    {
        ConnectorIterator itr = _connectorMap.find(connectorId);
        ToolBoxIterator itr1 = _toolBoxMap.find(lToolBoxId);
        ToolBoxIterator itr2 = _toolBoxMap.find(rToolBoxId);

        if( itr != _connectorMap.end() &&
            itr1 != _toolBoxMap.end() &&
            itr2 != _toolBoxMap.end())
        {
            itr.value()->setToolBox(itr2.value(), ME_ToolBox::RIGHT_TOOLBOX);

            itr1.value()->setToolBox(itr2.value(), ME_ToolBox::RIGHT_TOOLBOX);
            itr2.value()->setToolBox(itr1.value(), ME_ToolBox::LEFT_TOOLBOX);

            itr.value()->setConnectorState(ME_ToolBox::CONNECTED);
        }
        else
        {
            qCritical("_handleConnectionEstablishment() ToolBoxes or the Connector not found!");
        }
    }

    void
    RootAdapter::_handleToolBoxClosing(const QString& uniqueName)
    {
        _toolBoxMap.remove(uniqueName);
    }

    void
    RootAdapter::_destroyConnector(const QString& connectorId)
    {
        _connectorMap.remove(connectorId);
    }

    void
    RootAdapter::_handleButtonClick(QString buttonName)
    {
        if(buttonName=="")
        {
        }
    }

    void
    RootAdapter::showApp()
    {
        _rootView->show();

        QQmlContext *rootContext = _rootView->rootContext();
        QScreen* screenObj = _rootView->screen();

        int width = screenObj->availableGeometry().width();
        int height = screenObj->availableGeometry().height();
        _rootView->setWidth(width-100);
        _rootView->setHeight(height-100);
        _rootView->setX(50);
        _rootView->setY(50);
        _rootView->setTitle(MEConst::ProductName.c_str());

        rootContext->setContextProperty("applicationName", QVariant(MEConst::AppName.c_str()));

        /**** Code for using logic lib
        QSharedPointer<Logic::LogicLibFactory> factory =
                QSharedPointer<Logic::LogicLibFactory>(new Logic::LogicLibFactory);
        Logic::ILogicManager* obj = (Logic::ILogicManager*) factory->createObj("LogicManager");
        QSharedPointer<Logic::ILogicManager> logicMgr =
                QSharedPointer<Logic::ILogicManager>(obj);
        int val = logicMgr->addNumbers(3,6);
        */
    }
}
