#ifndef ME_ROOT_ADAPTER_
#define ME_ROOT_ADAPTER_

#include <QQmlContext>
#include "IRootAdapter.h"
#include "MEAdapter.h"

namespace ME_ToolBox
{
    class IToolBox;
    class IConnector;
    class ToolBoxFactory;
}

namespace ME_ToolFactory
{
    class METoolFactory;
}

namespace MEApp
{

    /// \typedef Iterators
    typedef QMap<QString, QSharedPointer<ME_ToolBox::IToolBox> >::iterator ToolBoxIterator;
    typedef QMap<QString, QSharedPointer<ME_ToolBox::IConnector> >::iterator ConnectorIterator;

    /// \class  RootAdapter
    /// \brief  The Main Adapater that instantiates other adapters and run the App
    class RootAdapter:  public QObject,
                        public IRootAdapter,
                        public MEAdapter
    {
    Q_OBJECT
    public:

        /// \brief  constructor
        RootAdapter(QObject* parent=0);

        ~RootAdapter();

        /// \brief  Initialize this adapter
        void initialize(QQmlApplicationEngine* engine, QQuickView* root);

        /// \brief  Show the application window
        void showApp();

        /// \brief  Create a new connector and add into the list,
        ///         Returns its uniqueId.
        ME_ToolBox::IConnector* _addNewConnectorToPlayGround();

    private slots:

        /// \brief  Slot for handling the closing of tool box
        void _handleToolBoxClosing(const QString& uniqueName);

        /// \brief  Slot for handling the removal of the connector
        void _destroyConnector(const QString& connectorId);

        /// \brief  Slot for main button click signals
        void _handleButtonClick(QString buttonName);

        /// \brief  Slot for creating a new tool box
        void _handleToolBoxCreation(QString name, int x, int y);

        /// \brief  Slot for creating and adding new connector to a Tool Box
        void _handleNewConnectorAddition(const QString& toolBoxName);

        /// \brief  Slot for establishing a connection
        void _handleConnectionEstablishment(const QString& connectorId, const QString& lToolBoxId, const QString& rToolBoxId);

    protected:

        /// \brief  A Map of Tool box Items added to the Playground, accessed
        ///         by a uniqueName provided to each of them
        QMap<QString, QSharedPointer<ME_ToolBox::IToolBox> > _toolBoxMap;

        /// \brief  A Map of Connector Items added to the Playground, accessed
        ///         by a uniqueId provided to each of them
        QMap<QString, QSharedPointer<ME_ToolBox::IConnector> > _connectorMap;

        /// \brief  Central place of the GUI where main activities happen
        QQuickItem* _playGroundItem;

        /// \brief  Instance of the ME_ToolFactory Obj
        QSharedPointer<ME_ToolFactory::METoolFactory> _meToolFactory;

        /// \brief  Instance of the ME_ToolFactory Obj
        QSharedPointer<ME_ToolBox::ToolBoxFactory> _toolBoxFactory;
    };
}


#endif // ME_ROOT_ADAPTER_
