#include <QScreen>
#include <assert.h>

#include "LayoutAdapter.h"

namespace MEApp
{
    void
    LayoutAdapter::initialize(QQmlApplicationEngine* engine, QQuickView* root)
    {
        MEAdapter::initialize(engine, root);

        QQmlComponent aTemplate(_engine, QUrl("qrc:///Layouts.qml"));
        _layoutQmlItem = qobject_cast<QQuickItem*>(aTemplate.create());
        assert(_layoutQmlItem);

        QQmlEngine::setObjectOwnership(_layoutQmlItem, QQmlEngine::CppOwnership);

        /*
        bool connected;
        connected = connect((QObject*)_layoutQmlItem,
                            SIGNAL(buttonSignal(QString)),
                            this,
                            SLOT(_handleButtonClick(QString)));

        assert(connected);
        */
    }

    QQuickItem*
    LayoutAdapter::getQMLItem()
    {
        return _layoutQmlItem;
    }

    void
    LayoutAdapter::_handleButtonClick(QString buttonName)
    {
    }
}
