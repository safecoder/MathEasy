#ifndef ME_IMEADAPTER_
#define ME_IMEADAPTER_

#include <QQmlApplicationEngine>
#include <QQuickView>

namespace MEApp
{
    class IMEAdapter{

    public:
        virtual ~IMEAdapter(){}

        /// \brief  Initialize this adapter
        virtual void initialize(QQmlApplicationEngine* engine, QQuickView* root) = 0;
    };
}


#endif // ME_IMEADAPTER_
