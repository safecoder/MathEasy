#ifndef ME_ILAYOUT_ADAPTER_
#define ME_ILAYOUT_ADAPTER_

#include <QQmlApplicationEngine>
#include <QQuickView>

namespace MEApp
{
    class ILayoutAdapter{

    public:
        virtual ~ILayoutAdapter(){}

        /// \brief  Initialize this adapter
        virtual void initialize(QQmlApplicationEngine* engine, QQuickView* root) = 0;

        /// \brief  Get the QML Item for its GUI
        virtual QQuickItem* getQMLItem() = 0;
    };
}


#endif // ME_ILAYOUT_ADAPTER_
