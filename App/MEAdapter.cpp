#include <QScreen>
#include <assert.h>

#include "MEAdapter.h"

namespace MEApp
{
    MEAdapter::~MEAdapter()
    {
    }

    void
    MEAdapter::initialize(QQmlApplicationEngine* engine, QQuickView* root)
    {
        _rootView = root;
        _engine = engine;
    }
}
