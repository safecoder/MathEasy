#ifndef ME_MEADAPTER_
#define ME_MEADAPTER_

#include "IMEAdapter.h"

namespace MEApp
{
    class MEAdapter:  public IMEAdapter
    {
    public:

        virtual ~MEAdapter();

        /// \brief  Initialize this adapter
        void initialize(QQmlApplicationEngine* engine, QQuickView* root);

    protected:
        /// \brief  Root window for the application
        QQuickView* _rootView;

        /// \brief  Application Engine
        QQmlApplicationEngine* _engine;

    };
}

#endif // ME_MEADAPTER_
