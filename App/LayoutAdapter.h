#ifndef ME_LAYOUT_ADAPTER_
#define ME_LAYOUT_ADAPTER_

#include <QQuickItem>
#include "ILayoutAdapter.h"
#include "MEAdapter.h"

namespace MEApp
{
    /// \class  LayoutAdapter
    /// \brief  Handles Layouts for displaying different tool boxes
    class LayoutAdapter: public QObject,
                            public ILayoutAdapter,
                            public MEAdapter
    {
    Q_OBJECT
    public:
        /// \brief  Initialize this adapter
        void initialize(QQmlApplicationEngine* engine, QQuickView* root);

        /// \brief  Get the QML Item for its GUI
        QQuickItem* getQMLItem();

    private slots:
        /// \brief  Slot for main button click signals
        void _handleButtonClick(QString buttonName);

    private:

        /// \brief  QML Item for the layouts section
        QQuickItem* _layoutQmlItem;

    };
}


#endif // ME_LAYOUT_ADAPTER_
