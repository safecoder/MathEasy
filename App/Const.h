#ifndef ME_CONST_
#define ME_CONST_

namespace MEConst
{
    const std::string AppName = "MathEasy";

    const std::string AppVersion = "1.0";

    const std::string ProductName = AppName + "-" + AppVersion;

}


#endif // ME_CONST_
