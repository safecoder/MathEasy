#ifndef LOGICMANAGER_H
#define LOGICMANAGER_H

#include "logiclib_global.h"
#include "ILogicManager.h"

namespace Logic
{
    class LogicManager : public ILogicManager
    {

    public:
        ~LogicManager();

        int addNumbers(int a, int b);
    };
}
#endif // LOGICMANAGER_H
