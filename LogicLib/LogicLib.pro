#-------------------------------------------------
#
# Project created by QtCreator 2015-05-10T18:14:54
#
#-------------------------------------------------

# build setup
include(../Config/build.pri)

QT       -= gui

TARGET = LogicLib
TEMPLATE = lib

DEFINES += LOGICLIB_LIBRARY

SOURCES +=  LogicManager.cpp \
            LogicLibFactory.cpp

HEADERS += LogicManager.h \
        logiclib_global.h \
        ILogicManager.h \
        LogicLibFactory.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
