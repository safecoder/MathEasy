#ifndef ILOGICFACTORY_H
#define ILOGICFACTORY_H

#include "logiclib_global.h"
#include <string>

namespace Logic
{

    class LOGICLIBSHARED_EXPORT LogicLibFactory
    {

    public:
        LogicLibFactory(){}
        void* createObj(const std::string& key);
    };
}

#endif // ILOGICFACTORY_H
