#ifndef ILOGICMANAGER_H
#define ILOGICMANAGER_H

#include "logiclib_global.h"

namespace Logic
{

    class LOGICLIBSHARED_EXPORT ILogicManager
    {

    public:        

        virtual ~ILogicManager(){}

        virtual int addNumbers(int a, int b) = 0;
    };
}

#endif // ILOGICMANAGER_H
