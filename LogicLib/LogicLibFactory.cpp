#include "LogicLibFactory.h"
#include "LogicManager.h"
#include <QDebug>
#include <assert.h>

namespace Logic
{
    void* LogicLibFactory::createObj(const std::string& key)
    {
        if(key=="LogicManager")
        {
            return (new LogicManager);
        }

        qCritical() << "LogicLibFactory::createObj: Key is not identified";
        assert(false);
        return NULL;
    }
}
