#include <MathFunction/MathFunctionTool.h>
#include <QThread>
#include <assert.h>

namespace ME_Functions
{
    MathFunction::MathFunction(QObject* parent):
    ToolBox(parent)
    {
    }

    MathFunction::~MathFunction()
    {
    }

    void
    MathFunction::initialize(QQmlApplicationEngine *engine, QQuickItem* parent, int x, int y)
    {
        ToolBox::initialize(engine, parent, x, y);

        // Create Functions Tool box QML Item
        QQmlComponent aTemplate(engine, QUrl("qrc:///Functions.qml"));
        _funcQmlItem = qobject_cast<QQuickItem*>(aTemplate.create());
        assert(_funcQmlItem);

        QQmlEngine::setObjectOwnership(_funcQmlItem, QQmlEngine::CppOwnership);

        assert(_guiItem);

        QObject* toolBoxContentItem = _guiItem->findChild<QObject*>("contentArea");
        if(toolBoxContentItem)
        {
            QQuickItem* parentItem = dynamic_cast<QQuickItem*>(toolBoxContentItem);
            _funcQmlItem->setParentItem(parentItem);
            parentItem->setWidth(_funcQmlItem->width());
            parentItem->setHeight(_funcQmlItem->height());
        }

        _guiItem->setProperty("hasLeftPin", QVariant( hasInputPin() ));
        _guiItem->setProperty("hasRightPin", QVariant( hasOutputPin() ));

        setTitle("MathFunctions");
    }

    bool MathFunction::hasInputPin()
    {
        return true;
    }

    bool MathFunction::hasOutputPin()
    {
        return true;
    }
}
