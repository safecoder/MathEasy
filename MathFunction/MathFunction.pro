#-------------------------------------------------
#
# Project created by QtCreator 2015-06-07T23:18:20
#
#-------------------------------------------------

# build setup
include(../Config/build.pri)

QT       += qml quick

QT       -= gui

TARGET = MathFunction
TEMPLATE = lib

DEFINES += MATHFUNCTION_LIBRARY

SOURCES += MathFunctionTool.cpp

HEADERS +=  MathFunctionTool.h\
            mathfunction_global.h \
            IMathFunctionTool.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$BUILD_PATH/release/ -lToolBox
else:win32:CONFIG(debug, debug|release): LIBS += -L$$BUILD_PATH/debug/ -lToolBox
else:unix: LIBS += -L$$BUILD_PATH/ -lToolBox

DEPENDPATH += $$PWD/../ToolBox
INCLUDEPATH += $$PWD/..

