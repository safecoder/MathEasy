#ifndef MATHFUNCTIONTOOL_H
#define MATHFUNCTIONTOOL_H

#include <MathFunction/mathfunction_global.h>
#include <MathFunction/IMathFunctionTool.h>
#include <ToolBox/ToolBox.h>

namespace ME_Functions
{
    class MATHFUNCTIONSHARED_EXPORT MathFunction:
                    public ME_ToolBox::ToolBox,
                    public IMathFunction
    {
    Q_OBJECT
    public:

        /// \brief  Construct Math Function Tool
        MathFunction(QObject* parent=0);

        /// \brief  Dtor
        ~MathFunction();

        /// \brief  Initialize this tool
        void initialize(QQmlApplicationEngine *engine, QQuickItem* parent, int x, int y);

        /// \brief  Does this tool box has input pin
        bool hasInputPin();

        /// \brief  Does this tool box has output pin
        bool hasOutputPin();

    protected:

        /// \brief  Functions Tool QML Item
        QQuickItem* _funcQmlItem;

    };

}

#endif // MATHFUNCTIONTOOL_H
