#ifndef IMATHFUNCTIONTOOL_H
#define IMATHFUNCTIONTOOL_H

#include <MathFunction/mathfunction_global.h>

namespace ME_Functions
{
    class MATHFUNCTIONSHARED_EXPORT IMathFunction
    {

    public:
        virtual ~IMathFunction(){}
    };
}

#endif // IMATHFUNCTIONTOOL_H
